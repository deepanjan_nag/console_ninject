﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEdemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Bind<IStringManipulator>().To<StringManipulator>();

            var sMani = kernel.Get<IStringManipulator>();
            new Launcher(sMani);
            Console.ReadLine();
            
        }
    }

    class Launcher
    {
        IStringManipulator sManipulator;

        public Launcher(IStringManipulator sManipulator)
        {
            this.sManipulator = sManipulator;
            
            Console.WriteLine(sManipulator.Reverser("Deepanjan"));
        }
    }


    class StringManipulator:IStringManipulator
    {
        public string Reverser(string str)
        {
            var charArray = str.ToCharArray();
            var revCharArray = charArray.Reverse().ToArray();
            var revString = new string(revCharArray);
            return revString;
        }
    }
    interface IStringManipulator
    {
        string Reverser(string str);
    }
}
